#pragma once
#include <Windows.h>
#include <iostream>
#include <string>
class pkeyhelper
{
private:
	HMODULE m_pkeyhelper;
	int get_eid(const std::wstring& edition_name);
public:
	pkeyhelper();
	~pkeyhelper();
	std::wstring get_pk(const std::wstring& edition_name, const std::wstring& channel);
};

