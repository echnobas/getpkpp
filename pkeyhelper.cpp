#include "pkeyhelper.h"

pkeyhelper::pkeyhelper() {
	this->m_pkeyhelper = LoadLibraryA("pkeyhelper.dll");
	if (!this->m_pkeyhelper)
		throw std::runtime_error{ "Couldn't load pkeyhelper.dll" };
}
pkeyhelper::~pkeyhelper() {
	if (this->m_pkeyhelper)
		FreeLibrary(this->m_pkeyhelper);
}

int pkeyhelper::get_eid(const std::wstring& edition_name) {
	void (*GetEditionIdFromName)(LPCWSTR, int*);
	GetEditionIdFromName = (decltype(GetEditionIdFromName))GetProcAddress(this->m_pkeyhelper, "GetEditionIdFromName");
	int eid;
	GetEditionIdFromName(edition_name.c_str(), &eid);
	return eid;
}

std::wstring pkeyhelper::get_pk(const std::wstring& edition_name, const std::wstring& channel) {
	int eid = this->get_eid(edition_name);
	int (*SkuGetProductKeyForEdition)(int, LPCWSTR, LPWSTR*, LPWSTR*);
	SkuGetProductKeyForEdition = (decltype(SkuGetProductKeyForEdition))GetProcAddress(this->m_pkeyhelper, "SkuGetProductKeyForEdition");
	LPWSTR product_key = nullptr;
	if (SkuGetProductKeyForEdition(eid, channel.c_str(), &product_key, nullptr) || !product_key)
		throw std::runtime_error{ "Getting product key failed!" };
	return std::wstring(product_key);
}