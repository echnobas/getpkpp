#include <iostream>
#include <thread>
#include <Windows.h>

#include "pkeyhelper.h"

int main()
{
    int exit_code = 0;

    std::wstring edition_id;
    std::wcout << "Edition :: " << std::flush;
    std::getline(std::wcin, edition_id);

    std::wstring channel;
    std::wcout << "Channel :: " << std::flush;
    std::getline(std::wcin, channel);

    try {
        pkeyhelper helper;
        std::wstring product_key = helper.get_pk(edition_id, channel);
        std::wcout << "   Key  :: " <<  product_key << std::endl;
    }
    catch (const std::runtime_error& e) {
        std::wcerr << "ERROR! " << e.what() << std::endl;
        exit_code = 1;
    }

    std::atomic_bool early_exit = false;
    std::thread t([&]() {
        std::wcin.get();
        early_exit = true;
        });
    t.detach();
    for (int i = 0; (i < 100) && !early_exit; i++)
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return exit_code;
}